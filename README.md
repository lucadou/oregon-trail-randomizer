# Oregon Trail II Randomizer

Have you ever been playing Oregon Trail II and thought "this game is too easy"?
Fear no more! Now you can be faced with sub-optimal conditions entirely out of
your control! Set out in August as an artist in a wagon accompanied by senior
citizens, get lucky and leave as a doctor in April with young-ish companions, or
anything in between! Simply load up the Oregon Trail II Randomizer, click in the
boxes, and hope you get a favorable result!

![A screenshot of the Oregon Trail Randomizer website](img/screenshot.png)

This application can be self-hosted, or you can access a live version [here.](https://ot2.monotonetim.tv/)

## Getting Started

### Prerequisites

* Any web server (this is all run client-side, so it works on GitHub/GitLab
  Pages, S3, Nginx, etc. out of the box)
* JavaScript enabled in the browser (for clients)

### Installation

* Open [index.html](html/index.html) in your editor of choice
* Replace every occurrence of `your.domain.here` with your domain (just the
  domain name)
* Replace the `twitter:site` meta tag's `@YourTwitterHandleHere` with your
  Twitter handle (or remove the line if you do not have/want to link a Twitter
  account)

### Known Issues

* On smaller screens, pressing the wagon/emote does not play the Oregon Trail
  2 title song (smaller screens being any screen where the wagon/emote ends up
  on the left side of the screen)
* Pressing the "Do not press" button a bunch does not make the page level
  (this was done on purpose by the original creator and is not a bug)

## FAQ

> What's with the Twitch Emote of the Day? And why is "Tim" the autofilled
> name?

This was originally created for the Twitch streamer
[MonotoneTim](https://www.twitch.tv/MonotoneTim), who played Oregon Trail II a
lot on her streams. The emotes that rotate around are her subscriber emotes,
with the exception of Kappa, a global Twitch emote.

> Did you make this?

No. [This](http://spookle.xyz/oregon/II/) is the original website that hosted
the randomizer. The original creator is pimanrules (pimanrules on Steam,
Twitch, Twitter, and YouTube).  
As far as I can tell, the first time the randomizer was used was on
[June 21, 2013](https://twitter.com/monotonetim/status/348253984395497473)
(credit for finding that goes to OLDMARIO on Twitch).

> Have you modified this?

Yes. The original site had all the CSS and most of the JS inlined, and I removed
most of it and put it in separate files. I added proper indentation, favicons,
and meta tags, fixed mobile formatting, and am working on enabling some
(currently) broken features.

> What is the project icon?

The project icon (on GitLab) and favicon is
[timWagon](https://www.twitchemotes.com/channels/21931814/emotes/46698),
one of MonotoneTim's
[Twitch emotes](https://www.twitchemotes.com/channels/21931814),
though it has been angled slightly.

> What is the license status of this project?

CC0. It's not really a software license, but it's what the original creator of
the randomizer wants.

> pimanrules: feel free to consider it CC0 for all I care, with the disclaimer
> that all the assets are stolen from the game  
> pimanrules: although the game is probably abandonware at this point so who
> cares  
> pimanrules: OSI doesn't recommend it but I don't care enough to find a
> software equivalent

-pimanrules, March 21, 2020 at 5:10 PM

## Versioning

We use Semantic Versioning, aka [SemVer](https://semver.org/spec/v2.0.0.html).

## Authors

* Luna Lucadou - Redid the randomizer to work better and on mobile devices
* pimanrules - Created the original randomizer

## License

This project is licensed under the CC0 ("No Rights Reserved") v1.0 - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgements

* pimanrules, whose [initial work](http://www.spookle.xyz/oregon/II/) is what I based this off of

